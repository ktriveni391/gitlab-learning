/* eslint-disable no-unused-vars */
import useStyles from "./AppStyles";
import { Route, Switch, BrowserRouter as Router } from "react-router-dom";
import AuthRoute from "./components/Route";
import { useEffect, useState } from "react";
import { ThemeProvider } from "@mui/material/styles";
import Box from "@mui/material/Box";
import service from "./service";
import constants from "./constants";
import { isEmpty } from "lodash";
import React from "react";
import ContactContainer from "./pages/contact/contactContainer";
import NewConversationContainer from "./pages/newConversation/newConversationContainer";
import ConversationContainer from "./pages/conversations/conversationContainer";
import Conversation from "./pages/conversation/conversationContainer";
import theme from "./theme";
import { Avatar } from "@mui/material";

function App() {
  const classes = useStyles();

  const [user, setUser] = useState({});

  useEffect(() => {
    console.log("Hola, I am here");
    if (!isEmpty(user))
      service.setHeader(constants.service.header.userId, user.id);
  }, [user]);



  return (
    <Box className={classes.container}>
      <Router>
        <Switch>
          <AuthRoute path="/newConversation" userId={user.id}>
            <NewConversationContainer user={user} />
          </AuthRoute>
          <AuthRoute path="/conversations" exact userId={user.id}>
            <ConversationContainer user={user} />
          </AuthRoute>
          <AuthRoute path="/conversation/:id" userId={user.id}>
            <Conversation userId={user.id} />
          </AuthRoute>
          <Route path="/">
            <ThemeProvider theme={theme}>
              <ContactContainer setUserId={setUser} />
            </ThemeProvider>
          </Route>
        </Switch>
      </Router>
    </Box>
  );
}

export default App;
