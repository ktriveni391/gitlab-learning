import { createTheme } from "@material-ui/core/styles";

// Create a theme instance.
const theme = createTheme({
  overrides: {
    MuiCssBaseline: {
      "@global": {
        html: {
          WebkitFontSmoothing: "auto",
        },
      },
    },
    MuiTypography: {
      h3: {
        color: "red",
      },
    },
    MuiListItem: {
      "& img": {
        color: "#757575",
      },
    },
  },
  typography: {
    fontFamily: ["system-ui", "-apple-system", "SF Pro Text"],
    h1: {
      fontFamily: "Roboto",
      fontSize: "50px",
      letterSpacing: "0.1px",
      lineHeight: "59px",
      color: "#000000",
      fontWeight: 400,
    },
    h2: {
      fontFamily: "Open sans",
      fontWeight: 600,
      fontSize: "14px",
      wordBreak: "break-word",
      letterSpacing: "0.1px",
      lineHeight: "19px",
    },
    h3: {
      fontSize: "14px",
      letterSpacing: "0.1px",
      wordBreak: "break-word",
      lineHeight: "19px",
      color: "#5F5F5F",
      fontWeight: 700,
    },
    h4: {
      fontFamily: "Open sans",
      fontWeight: 400,
      fontSize: "14px",
      wordBreak: "break-word",
      letterSpacing: "0.1px",
      lineHeight: "19px",
    },
    h5: {
      fontSize: "14px",
      letterSpacing: "0.1px",
      wordBreak: "break-word",
      lineHeight: "19px",
      color: "#5F5F5F",
      fontWeight: 400,
    },
    h6: {
      fontFamily: "Roboto",
      fontSize: "16px",
      letterSpacing: "0.1px",
      lineHeight: "19px",
      fontWeight: 400,
      color: "#000000",
    },

    caption: {
      fontFamily: "Open sans",
      fontWeight: 400,
      fontSize: "12px",
      wordBreak: "break-word",
      letterSpacing: "0.1px",
      lineHeight: "21px",
      color: "#989898",
    },
    button: {
      color: "red",
    },
  },
});

export default theme;
