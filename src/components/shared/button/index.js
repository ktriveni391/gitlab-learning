import React from "react";
import PropTypes from "prop-types";
import "./button.css";

const Button = ({ disabed, onClick, type, children }) => {
  return (
    <button disabed={disabed} onClick={onClick} className={`button ${type}`}>
      {children}
    </button>
  );
};
Button.defaultProps = {
  type: "primary",
};
Button.propTypes = {
  disabed: PropTypes.bool,
  onClick: PropTypes.func,
  type: ["primary"],
  children: PropTypes.node,
};

export default Button;
