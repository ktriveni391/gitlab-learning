import { Button } from "@mui/material";
import { Box } from "@mui/system";
import PropTypes from "prop-types";
import React from "react";

import useStyles from "./errorStyles";

const ErrorContainer = ({ refresh }) => {
  const classes = useStyles();
  return (
    <Box className={classes.errorContainer}>
      <Box className={classes.errorMessage}>Something went wrong</Box>
      <Button onClick={refresh}>Retry Again!!</Button>
    </Box>
  );
};

export default ErrorContainer;

ErrorContainer.propTypes = {
  refresh: PropTypes.func,
};
