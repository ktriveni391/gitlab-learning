import { makeStyles } from "@mui/styles";

const useStyles = makeStyles(() => ({
  errorContainer: {
    display: "flex",
    width: "100%",
    height: "100%",
    justifyContent: "center",
    flexDirection: "column",
    alignItems: "center",
  },
  errorMessage: {
    fontSize: "2rem",
    paddingBottom: "2rem",
    color: "red",
  },
}));

export default useStyles;
