import React from "react";
import { Redirect, Route } from "react-router";
import PropTypes from "prop-types";

const AuthRoute = (props) => {
  console.log("#########", props);
  const { userId } = props;
  if (!userId) return <Redirect to="/" />;

  return <Route {...props} />;
};

AuthRoute.propTypes = {
  userId: PropTypes.number,
};
export default AuthRoute;
