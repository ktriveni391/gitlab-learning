import React from "react";
import PropTypes from "prop-types";
import { ActionCableConsumer } from "react-actioncable-provider";

const ActionCable = ({ onReceived, children }) => {
  const handleReceived = (message) => {
    console.log("Message recieved", message)
    onReceived(message);
  };

  const onConnected = () => {
    console.log("connection established");
  };

  const onRejected = (e) => {
    console.log("connection onRejected", e);
  };


  const onDisconnected = (e) => {
    console.log("connection disconnects", e);
  };

  return (
    <ActionCableConsumer
      channel="NotificationsChannel"
      onReceived={handleReceived}
      onConnected={onConnected}
      onDisconnected={onDisconnected}
      onRejected={onRejected}
    >
      {children}
    </ActionCableConsumer>
  );
};

export default ActionCable;

ActionCable.propTypes = {
  onReceived: PropTypes.func.isRequired,
  children: PropTypes.node,
};
