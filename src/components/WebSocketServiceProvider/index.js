import ActionCable from "./ActionCableProvider";

export default {
  actionCable: ActionCable,
};
