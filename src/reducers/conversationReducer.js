import {
  CONVERSATIONS_FAILURE,
  CONVERSATIONS_LOADING,
  CONVERSATIONS_SUCCESS,
} from "../actions/constants";

const conversationReducer = (
  state = {
    loading: false,
    error: false,
    conversations: [],
  },
  action
) => {
  switch (action.type) {
    case CONVERSATIONS_LOADING:
      return {
        ...state,
        loading: true,
      };
    case CONVERSATIONS_SUCCESS:
      return {
        ...state,
        loading: false,
        conversations: action.payload,
      };
    case CONVERSATIONS_FAILURE:
      return {
        ...state,
        loading: false,
        error: false,
      };
    default:
      return state;
  }
};

export default conversationReducer;
