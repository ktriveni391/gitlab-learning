import {
  CONTACTS_FAILURE,
  CONTACTS_LOADING,
  CONTACTS_SUCCESS,
} from "../actions/constants";

const contactReducer = (
  state = {
    loading: false,
    error: false,
    contacts: [],
  },
  action
) => {
  switch (action.type) {
    case CONTACTS_LOADING:
      return {
        ...state,
        loading: true,
        error: false,
      };
    case CONTACTS_SUCCESS:
      return {
        ...state,
        loading: false,
        contacts: action.payload,
      };
    case CONTACTS_FAILURE:
      return {
        ...state,
        loading: false,
        error: true,
      };
    default:
      return state;
  }
};

export default contactReducer;
