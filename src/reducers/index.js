import { combineReducers } from "redux";
import contacts from "./contactsReducer";
import conversations from "./conversationReducer";

export default combineReducers({
  contacts,
  conversations,
});
