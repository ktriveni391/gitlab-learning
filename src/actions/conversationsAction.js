import service from "../service";
import {
  CONVERSATIONS_FAILURE,
  CONVERSATIONS_LOADING,
  CONVERSATIONS_SUCCESS,
} from "../actions/constants";
import constants from "../constants";

export const getConversations = (history) => {
  return async (dispatch) => {
    dispatch({ type: CONVERSATIONS_LOADING });
    try {
      const response = await service.serviceInstance.get("/conversations");
      if (history) {
        if (response.data && response.data.length) {
          history.push(constants.route.paths.conversations);
        } else {
          history.push(constants.route.paths.newConversation);
        }
      }
      dispatch({ type: CONVERSATIONS_SUCCESS, payload: response.data });
    } catch (ex) {
      console.error("Unable to fetch contacts", ex);
      dispatch({ type: CONVERSATIONS_FAILURE });
    }
  };
};
