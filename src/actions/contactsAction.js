import service from "../service";
import {
  CONTACTS_FAILURE,
  CONTACTS_LOADING,
  CONTACTS_SUCCESS,
} from "./constants";

export const getContacts = () => {
  return async (dispatch) => {
    dispatch({ type: CONTACTS_LOADING });
    try {
      const response = await service.serviceInstance.get("/contacts");
      console.log(response);
      dispatch({ type: CONTACTS_SUCCESS, payload: response.data });
    } catch (ex) {
      console.error("Unable to fetch contacts", ex);
      dispatch({ type: CONTACTS_FAILURE });
    }
  };
};
