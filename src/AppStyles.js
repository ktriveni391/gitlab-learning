import { makeStyles } from "@mui/styles";

const useStyles = makeStyles(() => ({
  heading: {
    fontSize: "50px",
    lineHeight: "59px",
    color: "#000000",
    fontFamily: "Roboto",
  },
  heading6: {
    fontSize: "16px",
    lineHeight: "19px",
    color: "#000000",
  },
  container: {
    width: "100%",
    height: "100%",
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
    minHeight: "100vh",
  },
}));

export default useStyles;
