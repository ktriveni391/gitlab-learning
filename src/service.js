import axios from "axios";
import constants from "./constants";

class Service {
  constructor() {
    if (Service._instance) {
      return Service._instance;
    } else {
      this.serviceInstance = axios.create({
        baseURL: constants.service.baseURL,
      });
      Service._instance = this;
    }
  }

  setHeader(key, value) {
    this.serviceInstance.defaults.headers.common[key] = value;
  }
}

const service = new Service();

export default service;
