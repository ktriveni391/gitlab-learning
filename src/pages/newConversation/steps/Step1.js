import React, { Fragment, useEffect, useState } from "react";
import PropTypes from "prop-types";
import { Box } from "@mui/system";
import { Avatar, Typography } from "@mui/material";

import useStyles from "../newConversationStyles";
import userSrc from "../../../images/icons/user.svg";
import Button from "../../../components/shared/button";
import { isEmpty } from "lodash";

const Step1 = ({
  user,
  contacts,
  setActiveStep,
  onSelect,
  selectedContact,
  conversations,
}) => {
  const [otherContacts, setOtherContacts] = useState([]);
  const classes = useStyles();

  useEffect(() => {
    const filteredContacts = contacts.filter(
      (contact) => user.id !== contact.id
    );
    setOtherContacts(filteredContacts);
  }, [contacts]);

  const onContactSelect = (index) => {
    onSelect(index, otherContacts[index]);
  };

  const createConversation = () => {
    setActiveStep();
  };

  return (
    <Fragment>
      <Box className={classes.subTitle}>You don’t have any conversations</Box>

      {(conversations || []).length && (
        <Box className={`${classes.heading} ${classes.heading2}`}>
          Select contacts to message
        </Box>
      )}
      <Box className={classes.step1ContactWrapper}>
        {(otherContacts || []).map((contact, index) => (
          <Box
            key={index}
            className={classes.contactContainer}
            onClick={() => onContactSelect(index)}
          >
            <Avatar
              src={userSrc}
              className={`${classes.image} ${
                selectedContact[index] &&
                contact.id === selectedContact[index].id &&
                classes.active
              }`}
            />
            <Typography variant="h6" className={classes.heading6}>
              {contact.name}
            </Typography>
          </Box>
        ))}
      </Box>
      {!isEmpty(selectedContact) && (
        <Box className={classes.buttonContainer}>
          <Button onClick={createConversation} type="primary">
            Continue
          </Button>
        </Box>
      )}
    </Fragment>
  );
};

Step1.propTypes = {
  contacts: PropTypes.array,
  setUserId: PropTypes.func,
  conversations: PropTypes.array,
  history: PropTypes.shape({
    push: PropTypes.func,
  }),
  user: PropTypes.object,
  setActiveStep: PropTypes.func,
  selectedContact: PropTypes.object,
  onSelect: PropTypes.func,
};

export default Step1;
