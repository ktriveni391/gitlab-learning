import React, { Fragment, useState } from "react";
import PropTypes from "prop-types";
import { Box } from "@mui/system";
import { Avatar, CircularProgress, TextField, Typography } from "@mui/material";

import userSrc from "../../../images/icons/user.svg";
import useStyles from "../newConversationStyles";
import Button from "../../../components/shared/button";
import constants from "../../../constants";
import service from "../../../service";
import { withRouter } from "react-router";
import ErrorContainer from "../../../components/shared/Error";

const Step2 = ({ selectedContact, history }) => {
  const classes = useStyles();
  const [groupName, setGroupName] = useState();
  const [loading, setLoading] = useState(false);
  const [error, setError] = useState(false);

  const getContactIds = () => {
    const contact_ids = [];
    for (let key in selectedContact) {
      if (Object.prototype.hasOwnProperty.call(selectedContact, key)) {
        const contact = selectedContact[key];
        contact_ids.push(contact.id);
      }
    }
    return contact_ids;
  };

  const createConversation = async () => {
    const contact_ids = await getContactIds();
    setLoading(true);
    setError(false);
    try {
      const res = await service.serviceInstance.post(`/conversations`, {
        title: groupName,
        contact_ids,
      });
      if (res.status === 200) {
        setLoading(false);
        history.push(`${constants.route.paths.conversation}${res.data.id}`);
      }
    } catch (ex) {
      setError(false);
      console.error("unable to create conversation", ex);
    }
  };

  const renderContacts = () => {
    const contactsComponent = [];
    for (let key in selectedContact) {
      if (Object.prototype.hasOwnProperty.call(selectedContact, key)) {
        const contact = selectedContact[key];

        contactsComponent.push(
          <Box key={key} className={classes.contactContainer}>
            <Avatar src={userSrc} className={classes.active} />
            <Typography className={classes.heading6}>{contact.name}</Typography>
          </Box>
        );
      }
    }
    return contactsComponent;
  };

  if (loading) {
    return (
      <Box className={classes.loadingContainer}>
        <CircularProgress />
      </Box>
    );
  }

  if (error) {
    return <ErrorContainer refresh={createConversation} />;
  }

  return (
    <Fragment>
      <Box className={classes.subTitle}>
        Give title to start a new conversation with 2 participants
      </Box>

      <Box className={classes.contactWrapper}>{renderContacts()}</Box>
      <Box className={classes.inputContainer}>
        <TextField
          placeholder="Enter your group name"
          value={groupName}
          width="100px"
          onChange={(e) => setGroupName(e.target.value)}
        />
      </Box>
      {groupName && (
        <Box className={classes.buttonContainer}>
          <Button type="primary" onClick={createConversation}>
            Start your Conversation
          </Button>
        </Box>
      )}
    </Fragment>
  );
};

Step2.propTypes = {
  selectedContact: PropTypes.object,
  history: PropTypes.object,
};

export default withRouter(Step2);
