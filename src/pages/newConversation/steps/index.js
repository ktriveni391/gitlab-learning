import React, { useState } from "react";
import PropTypes from "prop-types";
import { Box } from "@mui/material";

import Step1 from "./Step1";
import Step2 from "./Step2";
import useStyles from "../newConversationStyles";

const Step = ({ user, contacts, conversations }) => {
  const [activeStep, setActiveStep] = useState(1);
  const [selectedContact, setSelectedContact] = useState({});
  const classes = useStyles();

  const onSelect = (index, contact) => {
    setSelectedContact({ ...selectedContact, [index]: { ...contact } });
  };

  const renderSteps = () => {
    if (activeStep === 1) {
      return (
        <Step1
          setActiveStep={() => setActiveStep(activeStep + 1)}
          contacts={contacts}
          user={user}
          onSelect={onSelect}
          conversations={conversations}
          selectedContact={selectedContact}
        />
      );
    } else {
      return <Step2 selectedContact={selectedContact} />;
    }
  };

  return (
    <Box className={classes.container}>
      <Box className={classes.heading}>Welcome {user.name}!</Box>
      {renderSteps()}
    </Box>
  );
};

Step.propTypes = {
  contacts: PropTypes.array,
  user: PropTypes.object,
  conversations: PropTypes.array,
};

export default Step;
