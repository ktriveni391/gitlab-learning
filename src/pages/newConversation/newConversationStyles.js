import { makeStyles } from "@mui/styles";

const useStyles = makeStyles(() => ({
  contactWrapper: {
    marginTop: "80px",
    display: "flex",
    justifyContent: "space-between",
  },
  step1ContactWrapper: {
    marginTop: "80px",
    display: "flex",
    justifyContent: "space-between",
    flexDirection: "column",
  },
  subTitle: {
    color: "#5F5F5F",
    fontSize: "30px",
    lineHeight: "35px",
    paddingTop: "8px",
    textAlign: "center",
  },
  heading: {
    fontSize: "50px",
    lineHeight: "59px",
    color: "#000000",
    fontFamily: "Roboto",
    textAlign: "center",
  },
  heading2: {
    marginTop: "71px",
  },
  heading6: {
    fontSize: "16px",
    lineHeight: "19px",
    color: "#000000",
    textAlign: "center",
    paddingLeft: "6px",
  },
  container: {
    width: "45%",
    height: "100vh",
    marginTop: "61px",
    display: "flex",
    flexDirection: "column",
  },
  contactContainer: {
    display: "flex",
    paddingTop: "8px",
    cursor: "pointer",
    alignItems: "center",
    paddingBottom: "12px",
  },
  image: {
    marginRight: "16px",
  },
  active: {
    background: "#BCE3FF",
  },
  buttonContainer: {
    position: "fixed",
    bottom: "80px",
    right: "160px",
  },
  inputContainer: {
    position: "absolute",
    bottom: "80px",
    width: "40%",
  },
  loadingContainer: {
    height: "100%",
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
  },
}));

export default useStyles;
