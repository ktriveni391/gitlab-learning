import { CircularProgress } from "@mui/material";
import React, { useEffect } from "react";
import { connect, useDispatch } from "react-redux";
import PropTypes from "prop-types";
import { getContacts } from "../../actions/contactsAction";
import Step from "./steps";

const NewConversationContainer = ({
  contactLoading,
  contactErr,
  contacts,
  user,
  conversations,
}) => {
  const dispatch = useDispatch();

  const getContactsDetails = () => {
    dispatch(getContacts());
  };

  useEffect(() => {
    if (!contacts.length) {
      getContactsDetails();
    }
  }, []);

  if (contactLoading) {
    return <CircularProgress />;
  }
  if (contactErr) {
    return <NewConversationContainer refresh={getContactsDetails} />;
  }

  return <Step contacts={contacts} user={user} conversations={conversations} />;
};

const mapStateToProps = (store) => {
  return {
    contactLoading: store.contacts.loading,
    contactErr: store.contacts.error,
    contacts: store.contacts.contacts,
    conversation: store.conversations.conversations,
  };
};

NewConversationContainer.defaultProps = {
  contacts: [],
};
NewConversationContainer.propTypes = {
  contactErr: PropTypes.bool,
  contactLoading: PropTypes.bool,
  contacts: PropTypes.array,
  user: PropTypes.object,
  conversations: PropTypes.array,
};

export default connect(mapStateToProps)(NewConversationContainer);
