import { CircularProgress } from "@mui/material";
import React, { useEffect, useState } from "react";
import PropTypes from "prop-types";
import service from "../../service";
import { useParams } from "react-router";
import Conversation from "./conversation";
import ErrorContainer from "../../components/shared/Error";

const ConversationContainer = ({ userId }) => {
  const [loading, setLoading] = useState(false);
  const [error, setError] = useState(false);
  const [conversation, setConversation] = useState({});
  const params = useParams();

  const getConversation = async () => {
    setLoading(true);
    setError(false);

    try {
      const res = await service.serviceInstance.get(
        `/conversations/${params.id}`
      );
      setConversation(res.data);
      setLoading(false);
    } catch (ex) {
      setError(true);
      setLoading(false);
    }
  };

  useEffect(() => {
    getConversation();
  }, []);

  if (loading) {
    return <CircularProgress />;
  }
  if (error) {
    return <ErrorContainer refresh={getConversation} />;
  }
  return <Conversation conversation={conversation} userId={userId} />;
};

ConversationContainer.propTypes = {
  userId: PropTypes.string,
};

export default ConversationContainer;
