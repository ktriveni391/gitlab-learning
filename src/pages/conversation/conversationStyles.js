import { makeStyles } from "@mui/styles";

const useStyles = makeStyles(() => ({
  heading: {
    fontSize: "50px",
    lineHeight: "59px",
    color: "#000000",
    fontFamily: "Roboto",
  },
  heading6: {
    fontSize: "16px",
    lineHeight: "19px",
    color: "#000000",
    marginLeft: "10px",
  },
  container: {
    width: "80%",
    display: "flex",
    alignItems: "flex-start",
    marginTop: "68px",
    height: "100vh",
    flexDirection: "column",
  },
  messageContainer: {
    display: "flex",
    paddingTop: "8px",
    cursor: "pointer",
    margin: "12px",
    alignItems: "center",
  },
  messageWrapper: {
    width: "60%",
    marginTop: "39px",
  },
  active: {
    background: "#EDF7FF",
  },
  sender: {
    marginLeft: "10px",
    color: "#5F5f5f",
  },
  newMessageContainer: {
    position: "fixed",
    width: "101vw",
    bottom: "0",
    background: "#fff",
    marginLeft: "-11%",
    opacity: "100",
    zIndex: 100,
    padding: "1rem",
    paddingLeft: "10%",
    display: "grid",
    gridTemplateColumns: "70% 30%",
    "& input": {
      width: "100%",
      padding: "1rem",
    },
  },
  textFieldContainer: {
    width: "85%",
    position: "relative",
  },
}));

export default useStyles;
