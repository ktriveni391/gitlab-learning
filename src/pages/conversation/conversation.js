import React, { useEffect, useRef, useState } from "react";
import PropTypes from "prop-types";
import { Box } from "@mui/system";
import { Avatar } from "@mui/material";

import Button from "../../components/shared/button";
import useStyles from "./conversationStyles";
import user from "../../images/icons/user.svg";
import service from "../../service";
import webSockets from "../../components/WebSocketServiceProvider";
import constants from "../../constants";
import { isEmpty } from "lodash";

const Conversation = ({ conversation, userId }) => {
  const [recentMessages, setRecentMessages] = useState([]);
  const [newMessage, setNewMessage] = useState();
  const classes = useStyles();
  const WebSocketProvider = webSockets[constants.wsType];
  const ref = useRef();
  useEffect(() => {
    if (recentMessages.length && ref) {
      ref.current.focus();
    }
  }, [newMessage]);

  const sendMessage = async () => {
    try {
      await service.serviceInstance.post(
        `conversations/${conversation.id}/messages`,
        { content: newMessage }
      );
    } catch (ex) {
      console.error("#######", ex);
    }
  };

  useEffect(() => {
    if (!isEmpty(conversation)) {
      setRecentMessages([...(conversation.recent_messages || [])]);
    }
  }, [conversation]);

  const handleReceiveMessages = (newMessage) => {
    
    const isAvailble = recentMessages.every(
      (message) => message.id !== newMessage.id
    );
    if (isAvailble || !recentMessages.length) {
      recentMessages.push({ ...newMessage });
      setRecentMessages([...recentMessages]);
     
    }
  };

  return (
    <WebSocketProvider onReceived={handleReceiveMessages}>
      <Box className={classes.container}>
        <Box className={classes.heading}>{conversation.title}</Box>
        <Box className={classes.messageWrapper}>

          {recentMessages.map((message) => (
            <Box
              key={message.id}
              className={`${classes.messageContainer} ${
                userId === message.sender_id && classes.active
              }`}
            >
              <Avatar src={user} />
              <Box>
                <Box className={classes.heading6}>{message.content}</Box>
                <Box className={classes.sender}>
                  {userId === message.sender_id ? "you" : message.sender_name}
                </Box>
              </Box>
            </Box>
          ))}
          <Box className={classes.newMessageContainer}>
            <Box className={classes.textFieldContainer}>
              <input
                ref={ref}
                placeholder="Type here"
                fullWidth={true}
                value={newMessage}
                onChange={(e) => {
                  setNewMessage(e.target.value);
                }}
              />
            </Box>
            <Button onClick={sendMessage}>Send</Button>
          </Box>
        </Box>
      </Box>
    </WebSocketProvider>
  );
};

Conversation.propTypes = {
  conversation: PropTypes.object,
  userId: PropTypes.string,
};
export default Conversation;
