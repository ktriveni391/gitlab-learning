import { CircularProgress } from "@mui/material";
import React, { useEffect } from "react";
import { connect, useDispatch } from "react-redux";
import PropTypes from "prop-types";
import Contacts from "./contacts";
import { getContacts } from "../../actions/contactsAction";
import ErrorContainer from "../../components/shared/Error";

const ContactContainer = ({
  setUserId,
  contactLoading,
  contactErr,
  conversationErr,
  conversationLoading,
}) => {
  const dispatch = useDispatch();

  const getContactsDetails = () => {
    dispatch(getContacts());
  };

  useEffect(() => {
    getContactsDetails();
  }, []);

  if (contactLoading || conversationLoading) {
    return <CircularProgress />;
  }
  if (contactErr || conversationErr) {
    return <ErrorContainer refresh={getContactsDetails} />;
  }
  return <Contacts setUserId={setUserId} />;
};

const mapStateToProps = (store) => {
  return {
    contactLoading: store.contacts.loading,
    conversationLoading: store.conversations.loading,
    contactErr: store.contacts.error,
    conversationErr: store.conversations.error,
  };
};
ContactContainer.propTypes = {
  setUserId: PropTypes.func,
  conversationErr: PropTypes.bool,
  contactErr: PropTypes.bool,
  conversationLoading: PropTypes.bool,
  contactLoading: PropTypes.bool,
};

export default connect(mapStateToProps)(ContactContainer);
