import { Avatar, Typography } from "@mui/material";
import Box from "@mui/material/Box";
import React, { useState } from "react";
import { connect, useDispatch } from "react-redux";
import Button from "../../components/shared/button";
import useStyles from "./contactStyles";
import user from "../../images/icons/user.svg";
import { getConversations } from "../../actions/conversationsAction";
import PropTypes from "prop-types";
import { withRouter } from "react-router";
import { isEmpty } from "lodash";

const Contacts = ({ contacts, setUserId, history }) => {
  const classes = useStyles();
  const dispatch = useDispatch();
  const [selectedContact, setSelectedContact] = useState();

  const onContinue = () => {
    dispatch(getConversations(history));
  };

  return (
    <Box className={classes.container}>
      <Typography variant="h1">Let us know who you are.</Typography>
      <Box className={classes.contactWrapper}>
        {(contacts || []).map((contact, index) => (
          <Box
            key={index}
            className={classes.contactContainer}
            onClick={() => {
              setSelectedContact(contact);
              setUserId(contact);
            }}
          >
            <Avatar
              src={user}
              className={`${classes.image} ${
                selectedContact &&
                contact.id === selectedContact.id &&
                classes.active
              }`}
            />
            <Typography variant="h6">{contact.name}</Typography>
          </Box>
        ))}
      </Box>
      <Box className={classes.buttonContainer}>
        {!isEmpty(selectedContact) && (
          <Button onClick={onContinue}>Continue</Button>
        )}
      </Box>
    </Box>
  );
};

Contacts.propTypes = {
  contacts: PropTypes.array,
  setUserId: PropTypes.func,
  conversations: PropTypes.array,
  history: PropTypes.shape({
    push: PropTypes.func,
  }),
  loading: PropTypes.bool,
};
const mapStateToProps = (store) => {
  return {
    contacts: store.contacts.contacts,
    conversations: (store.conversations || {}).conversations,
    loading: (store.conversations || {}).loading,
  };
};

export default connect(mapStateToProps)(withRouter(Contacts));
