import { makeStyles } from "@material-ui/core";

const styles = makeStyles(() => {
  return {
    heading: {
      fontSize: "50px",
      lineHeight: "59px",
      color: "#000000",
      fontFamily: "Roboto",
    },
    name: {
      fontFamily: "Roboto",
      fontSize: "16px",
      letterSpacing: "0.1px",
      lineHeight: "19px",
      fontWeight: 400,
      color: "#000000",
    },
    heading6: {
      fontSize: "16px",
      lineHeight: "19px",
      color: "#000000",
    },
    contactWrapper: {
      marginTop: "80px",
      position: "relative",
    },
    container: {
      width: "100%",
      display: "flex",
      alignItems: "center",
      justifyContent: "center",
      flexDirection: "column",
    },
    contactContainer: {
      display: "flex",
      paddingTop: "8px",
      cursor: "pointer",
      paddingBottom: "12px",
    },
    image: {
      marginRight: "16px",
    },
    active: {
      background: "#BCE3FF",
    },
    buttonContainer: {
      position: "absolute",
      bottom: "75px",
      right: "160px",
    },
  };
});

export default styles;
