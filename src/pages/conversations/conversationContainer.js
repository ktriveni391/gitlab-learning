import { CircularProgress } from "@mui/material";
import React, { useEffect } from "react";
import { connect, useDispatch } from "react-redux";
import PropTypes from "prop-types";
import Conversations from "./conversations";
import { getConversations } from "../../actions/conversationsAction";
import ErrorContainer from "../../components/shared/Error";

const ConversationContainer = ({
  conversationErr,
  conversationLoading,
  conversations,
}) => {
  const dispatch = useDispatch();

  const getConversationsDetails = () => {
    dispatch(getConversations());
  };

  useEffect(() => {
    getConversationsDetails();
  }, []);

  if (conversationLoading) {
    return <CircularProgress />;
  }
  if (conversationErr) {
    return <ErrorContainer refresh={getConversationsDetails} />;
  }
  return <Conversations conversations={conversations} />;
};

const mapStateToProps = (store) => {
  return {
    conversationLoading: store.conversations.loading,
    conversationErr: store.conversations.error,
    conversations: store.conversations.conversations,
  };
};
ConversationContainer.propTypes = {
  conversationErr: PropTypes.bool,
  conversationLoading: PropTypes.bool,
  conversations: PropTypes.array,
};

export default connect(mapStateToProps)(ConversationContainer);
