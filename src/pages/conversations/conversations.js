import { Box } from "@mui/system";
import React from "react";
import PropTypes from "prop-types";

import { Avatar, Typography } from "@mui/material";
import useStyles from "./conversationStyles";
import user from "../../images/icons/user.svg";
import Button from "../../components/shared/button";
import { withRouter } from "react-router";
import constants from "../../constants";

const Conversations = ({ conversations, history }) => {
  const classes = useStyles();

  const openSelectedConversation = (conversation) => {
    history.push(`${constants.route.paths.conversation}${conversation.id}`);
  };

  const openCreateConversation = () => {
    history.push(`${constants.route.paths.newConversation}`);
  };

  return (
    <Box className={classes.container}>
      <Box className={classes.heading}>Your Conversations</Box>
      <Box className={classes.wrapper}>
        {(conversations || []).map((conversation, index) => (
          <Box
            key={index}
            className={classes.contactContainer}
            onClick={() => {
              openSelectedConversation(conversation);
            }}
          >
            <Avatar src={user} className={classes.image} />
            <Box className={classes.content}>
              <Typography variant="h6">{conversation.title}</Typography>
              <Box className={classes.sender}>
                {((conversation.last_message || [])[0] || {}).sender_name}
              </Box>

              <Box className={classes.messageContent}>
                {((conversation.last_message || [])[0] || {}).content}
              </Box>
            </Box>
          </Box>
        ))}
      </Box>
      <Box className={classes.buttonContainer}>
        <Button onClick={openCreateConversation}>
          Create New Conversation
        </Button>
      </Box>
    </Box>
  );
};

Conversations.propTypes = {
  conversations: PropTypes.array,
  history: PropTypes.object,
};
export default withRouter(Conversations);
