import { makeStyles } from "@mui/styles";

const useStyles = makeStyles(() => ({
  sender: {
    fontWeight: 700,
    fontSize: "14px",
    lineHeight: "16.41px",
    color: "#5F5F5F",
  },
  messageContent: {
    fontWeight: 400,
    fontSize: "14px",
    lineHeight: "16.41px",
    color: "#5F5F5F",
  },
  wrapper: {
    marginTop: "38px",
    width: "75%",
  },
  heading: {
    fontSize: "50px",
    lineHeight: "59px",
    color: "#000000",
    fontFamily: "Roboto",
  },
  heading6: {
    fontSize: "16px",
    lineHeight: "19px",
    color: "#000000",
  },
  container: {
    width: "80%",
    display: "flex",
    alignItems: "flex-start",
    marginTop: "68px",
    flexDirection: "column",
  },
  contactContainer: {
    display: "flex",
    paddingTop: "8px",
    cursor: "pointer",
    paddingBottom: "12px",
    alignItems: "center",
    "&:hover": {
      background: "#EDF7FF",
    },
  },
  image: {
    marginRight: "16px",
  },
  active: {
    background: "#BCE3FF",
  },
  content: {
    display: "flex",
    flexDirection: "column",
    marginLeft: "16px",
  },
  buttonContainer: {
    position: "absolute",
    bottom: "75px",
    right: "160px",
  },
}));

export default useStyles;
