import React from "react";
import ReactDOM from "react-dom";
import ActionCable from "actioncable";

import "./index.css";
import App from "./App";
import reportWebVitals from "./reportWebVitals";
import { ActionCableProvider } from "react-actioncable-provider";
import { Provider } from "react-redux";
import store from "./store";
import { CssBaseline, ThemeProvider } from "@material-ui/core";
import theme from "./theme";
import globalStyles from "./globalStyles";

const cable = ActionCable.createConsumer("ws://34.122.252.114:3000/cable");

ReactDOM.render(
  <React.StrictMode>
    <Provider store={store}>
      <ActionCableProvider cable={cable}>
        <ThemeProvider theme={theme}>
          <CssBaseline styles={globalStyles} />
          <App />
        </ThemeProvider>
      </ActionCableProvider>
    </Provider>
  </React.StrictMode>,
  document.getElementById("root")
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
