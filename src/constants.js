export default {
  service: {
    baseURL: "http://34.122.252.114:3000/",
    header: {
      userId: "user_id",
    },
  },
  route: {
    paths: {
      newConversation: "/newConversation",
      conversations: "/conversations",
      conversation: "/conversation/",
    },
  },
  wsType: "actionCable",
};
